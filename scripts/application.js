//Emergency Department Simulation

var os = com.jahova.os.Instance();
var util = com.jahova.utilities.Instance();
var console = util.Console.Instance();

var ED ={
    Entities: {
      ed: null,
      dr: null,
      nurse: null,
      patient: null,
      doctors: [],
      nurses: [],
      patients: []
    },
    Resources: {
        Rooms: {
            FT: [],
            RNT: [],
            Urgent: [],
            Waiting: {
                FT: [],
                RNT: [],
                Urgent: []
            }
        }
    },
    PendingOrders: {
        FT: [],
        RNT: {
            nurse: [],
            doctor: []
        },
        Urgent: {
            nurse: [],
            doctor: []
        }
        
    },
    Draw: function(){
        ED.Entities.ed.Graphics.Draw();
        //ED.Entities.dr.Graphics.Draw();
        
        //Draw Doctors
        for(var i = ED.Entities.doctors.length - 1; i >= 0; i-- ){
            ED.Entities.doctors[i].Graphics.Draw();
        }
        
        //Draw Nurses
        for(var i = ED.Entities.nurses.length - 1; i >= 0; i-- ){
            ED.Entities.nurses[i].Graphics.Draw();
        }
        
        //Draw Patients
        for(var i = ED.Entities.patients.length - 1; i >= 0; i-- ){
            ED.Entities.patients[i].Graphics.Draw();
        }
    },
    Update: function(dt){
        if(ED.Debug.enabled){
            ED.Debug.Update(dt);
        }
        
        os.input.Update(dt);
        os.ai.Update(dt);
        ED.Input.Update(dt);
        
        //Check for new patient arrivals
        
        //Check Moving Entities for Next Node in Path
        ED.UpdateEntityPaths();
        
        //Update FSM of Entities
        ED.UpdateEntityFSM(dt);
        
    },
    UpdateEntityFSM: function(dt){
        //Doctors
        for(var i = ED.Entities.doctors.length - 1; i >= 0; i--){
            ED.Entities.doctors[i].FSM.Update(dt);
        }
        
        //Nurses
        for(var i = ED.Entities.nurses.length - 1; i >= 0; i--){
            ED.Entities.nurses[i].FSM.Update(dt);
        }
        
        //Patients
        for(var i = ED.Entities.patients.length - 1; i >= 0; i--){
            ED.Entities.patients[i].FSM.Update(dt);
        }
    },
    UpdateEntityPaths: function(){
        //Doctors
        for(var i = ED.Entities.doctors.length - 1; i >= 0; i--){
            if(ED.Entities.doctors[i].path.length > 0){
                //If Dr does not have a target, then assign one
                if(!ED.Entities.doctors[i].AI.MovementAlgorithms.ArrivePosition){
                    os.ai.Movement.Add.Arrive.Position(ED.Entities.doctors[i], (ED.Entities.doctors[i].path.pop()).Position);
                }
            }
        }
        
        //Nurses
        for(var i = ED.Entities.nurses.length - 1; i >= 0; i--){
            if(ED.Entities.nurses[i].path.length > 0){
                //If Dr does not have a target, then assign one
                if(!ED.Entities.nurses[i].AI.MovementAlgorithms.ArrivePosition){
                    os.ai.Movement.Add.Arrive.Position(ED.Entities.nurses[i], (ED.Entities.nurses[i].path.pop()).Position);
                }
            }
        }
        
        //Patients
        for(var i = ED.Entities.patients.length - 1; i >= 0; i--){
            if(ED.Entities.patients[i].path.length > 0){
                //If Dr does not have a target, then assign one
                if(!ED.Entities.patients[i].AI.MovementAlgorithms.ArrivePosition){
                    os.ai.Movement.Add.Arrive.Position(ED.Entities.patients[i], (ED.Entities.patients[i].path.pop()).Position);
                }
            }
        }
    },
    GraphicReset: function(){
        
    },
    Debug: {
        enabled: false,
        Window: null,
        time: 0,
        numOfFrames: 0,
        Watch: null,
        Update: function(dt){
            ED.Debug.time += dt
            ED.Debug.numOfFrames++;
            
            //Update FPS
            if(ED.Debug.time > 1000){
                
                ED.Debug.time = ED.Debug.time / 1000;
                ED.Debug.Window.Set.statusbarText("FPS: " + (ED.Debug.numOfFrames/ED.Debug.time).toFixed(3));
                ED.Debug.time = 0;
                ED.Debug.numOfFrames = 0;
                
                
            }
            ED.Debug.Window.elements.content.html().innerHTML = "";
            
            if(ED.Entities.dr.path.length > 0){
                //If Dr does not have a target, then assign one
                if(!ED.Entities.dr.AI.MovementAlgorithms.ArrivePosition){
                    os.ai.Movement.Add.Arrive.Position(ED.Entities.dr, (ED.Entities.dr.path.pop()).Position);
                }
            }
            //Print Registered Debug Strings
            for(var i = ED.Debug.Watch.size; i--; ED.Debug.Watch.next()){
                ED.Debug.Window.elements.content.html().innerHTML += (ED.Debug.Watch.value())() + "</br>";
            }
        }
    },
    Init: function(){
        //os.debugbar.AnchorConsole();
        os.console.Comment("Initializing Emergency Department Simulation");
        
        ED.Load.Graphics();
        ED.Load.Assets();
        ED.Load.Map();
        ED.Load.AI();
        ED.Load.States();
        ED.Load.Entities();
        ED.Load.Controls();
        ED.Load.Resources();
        
        
        //
        //  DEBUG LOADING
        //
        ED.Load.DebugWindow();
        //ED.Entities.dr.yaw = 180;
        //ED.Entities.dr.path = [];
        //vec3.set((ED.Map.Nodes.get("Start")).Position,ED.Entities.dr.Position);
        //os.graphics.Managers.Camera.Attach(ED.Entities.dr);
        //os.graphics.Managers.Camera.Offset[0] = 0;
        //os.graphics.Managers.Camera.Offset[1] = 3;
        //os.graphics.Managers.Camera.Offset[2] = 8;
        os.graphics.Managers.Camera.Position = [0,1,-10];
        //
        //
        os.input.Register.Keyboard.Event.Keydown("N", ED.CreatePatient);
        os.graphics.Start();
    },
    Input: {
        Update: function(dt){
            var gp = os.input.Gamepads.current.get(0);
            gp = gp ? gp : os.input.Gamepads.current.get(1);
        
            if(gp){
                var cam = os.graphics.Managers.Camera;
                if(Math.abs(gp.RightStick.X()) > 0.2){
                    cam.Rotation.yaw += gp.RightStick.X() * 2.5;
                    
                    if(cam.Rotation.yaw > 360){ cam.Rotation.yaw  -= 360;}
                    else if(cam.Rotation.yaw < 0) { cam.Rotation.yaw += 360; }
                    
                }
                if(Math.abs(gp.RightStick.Y()) > 0.2){
                    cam.Rotation.pitch += gp.RightStick.Y() * 1.5;
                    
                    if(cam.Rotation.pitch > 360){ cam.Rotation.pitch  -= 360}
                    else if(cam.Rotation.pitch < 0) { cam.Rotation.pitch += 360; }
                    
                }
                if(Math.abs(gp.LeftStick.X()) > 0.2){
                    os.graphics.Managers.Camera.MoveRight(gp.LeftStick.X() * 5); 
                }
                if(Math.abs(gp.LeftStick.Y()) > 0.2){
                    os.graphics.Managers.Camera.MoveBackward(gp.LeftStick.Y() * 5); 
                }
                
                
                //
                //  DEBUG INPUT
                //
                
                if(gp.Dpad.Up() >= 1){
                    ED.Entities.dr.Move.Forward(0.1);
                }
                else if(gp.Dpad.Down() > 0.2){
                    ED.Entities.dr.Move.Backward(0.1);
                }
                else if(gp.Dpad.Left() > 0.2){
                    ED.Entities.dr.Move.Left(0.1);
                }
                else if(gp.Dpad.Right() > 0.2){
                    ED.Entities.dr.Move.Right(0.1);
                }
                
                if(gp.Buttons.Top()){// && !ED.Entities.dr.AI.MovementAlgorithms.ArrivePosition){
                    //Get path from entrance to Fast Track Room 8 (FT8)
                    //ED.Entities.dr.path = ED.Map.GetPath("Entrance", "FT8");
                    //os.ai.Movement.Add.Arrive.Position(ED.Entities.dr, (ED.Entities.dr.path.pop()).Position);
                    vec3.set([70.72,177.2,22.29], os.graphics.Managers.Camera.Position);
                    os.graphics.Managers.Camera.Rotation.yaw = 270.03;
                    os.graphics.Managers.Camera.Rotation.pitch = 66.48;
                }
                else if(gp.Buttons.Bottom()){// && !ED.Entities.dr.AI.MovementAlgorithms.ArrivePosition){
                    //Get path from Fast Track Room 8 (FT8) to Entrance
                    //ED.Entities.dr.path = ED.Map.GetPath("FT8", "Entrance");
                    //os.ai.Movement.Add.Arrive.Position(ED.Entities.dr, (ED.Entities.dr.path.pop()).Position);
                    vec3.set([70.72,177.2,22.29], os.graphics.Managers.Camera.Position);
                    os.graphics.Managers.Camera.Rotation.yaw = 270.03;
                    os.graphics.Managers.Camera.Rotation.pitch = 66.48;
                }
                else if(gp.Buttons.Left()){// && !ED.Entities.dr.AI.MovementAlgorithms.ArrivePosition){
                    //Get path from Fast Track Room 8 (FT8) to Entrance
                    //ED.Entities.dr.path = ED.Map.GetPath("FT8", "Entrance");
                    //os.ai.Movement.Add.Arrive.Position(ED.Entities.dr, (ED.Entities.dr.path.pop()).Position);
                    vec3.set([41.9,77.45,22.3], os.graphics.Managers.Camera.Position);
                    os.graphics.Managers.Camera.Rotation.yaw = 270.03;
                    os.graphics.Managers.Camera.Rotation.pitch = 82.08;
                }
                else if(gp.Buttons.Right()){// && !ED.Entities.dr.AI.MovementAlgorithms.ArrivePosition){
                    //Get path from Fast Track Room 8 (FT8) to Entrance
                    //ED.Entities.dr.path = ED.Map.GetPath("FT8", "Entrance");
                    //os.ai.Movement.Add.Arrive.Position(ED.Entities.dr, (ED.Entities.dr.path.pop()).Position);
                    vec3.set([-36.2,96.57,29.27], os.graphics.Managers.Camera.Position);
                    os.graphics.Managers.Camera.Rotation.yaw = 89.2;
                    os.graphics.Managers.Camera.Rotation.pitch = 87.1;
                }
            }
        }
    },
    Load: {
        Graphics: function(){
            //Load WebGL (Debug: false, fullscreen: true)
            os.graphics.Load(false, true);
            os.graphics.Set.Callback.Draw(ED.Draw);
            os.graphics.Set.Callback.Update(ED.Update);
            os.graphics.Set.Callback.Reset(ED.GraphicReset);
        },
        DebugWindow: function(){
            ED.Debug.enabled = true;
            ED.Debug.Window = os.windows.WindowsManager.Create.Window("ED Simulation", "PC");
            ED.Debug.Window.Set.position(0,0);
            //ED.Debug.Window.elements.content.html().style.overflow = "hidden";
            ED.Debug.Window.Set.statusbarText("");
            ED.Debug.Window.Display.window();
            ED.Debug.Window.Set.width(300);
            ED.Debug.Window.Set.height(300);
            ED.Debug.Window.Hide.menubar();
            ED.Debug.Window.Set.onMax(os.graphics.OnReset);
            ED.Debug.Window.Set.onMin(os.graphics.Pause);
            
            ED.Debug.Watch = os.resschmgr.Create.Map();
            
            ED.Debug.Window.Add = function(sKey, func){
                ED.Debug.Watch.put(sKey, func);
            }
            
            ED.Debug.Window.Remove = function(sKey){
                ED.Debug.Watch.remove(sKey);
            }
            
            ED.Debug.Window.Add("position", function(){ return "X: " + ED.Entities.dr.Position[0].toFixed(3) + ", Z: " + ED.Entities.dr.Position[2].toFixed(3)});

        },
        Assets: function(){
            //
            //  Load Meshes
            //
            var ed = os.graphics.Managers.Mesh.Create.Mesh("ed", "data/ed.json");
            ed.onLoad = function(){os.console.Comment("ED Mesh Loaded")};
            
            var doctor = os.graphics.Managers.Mesh.Create.Mesh("doctor", "data/doctor.json");
            doctor.onLoad = function(){os.console.Comment("Doctor Mesh Loaded")};
            
            var nurse = os.graphics.Managers.Mesh.Create.Mesh("nurse", "data/nurse.json");
            nurse.onLoad = function(){os.console.Comment("Nurse Mesh Loaded")};
            
            var patient = os.graphics.Managers.Mesh.Create.Mesh("patient", "data/joe.json");
            patient.onLoad = function(){os.console.Comment("Patient Mesh Loaded")};
            
            //
            //  Load textures
            //
            var edTex = os.graphics.Managers.Texture.Create.Texture("ed", "data/hospitalUV.bmp");
            edTex.onLoad = function(){os.console.Comment("ED Texture Loaded")};
             
            var people = os.graphics.Managers.Texture.Create.Texture("people", "data/littlebuddies.png");
            people.onLoad = function(){os.console.Comment("People Texture Loaded")};
            
        },
        Entities: function(){
            //
            //  Doctors
            //
                //Urgent
            ED.Entities.dr = os.graphics.Managers.Entity.Create();
            ED.Entities.dr.Graphics.Add.Mesh("doctor");
            ED.Entities.dr.Graphics.Add.Texture("people");
            ED.Entities.dr.AI = os.ai.Create.Entity();
            ED.Entities.dr.FSM = os.ai.Create.FSM(ED.Entities.dr);
            ED.Entities.dr.path = [];
            ED.Entities.doctors[0] = ED.Entities.dr;
            ED.Entities.dr.home = [-30.4,0,25.5];
            ED.Entities.dr.triage = "Urgent";
            ED.Entities.dr.location = "UrgentNurseStation";
            ED.Entities.dr.FSM.Transition("DSIdle");
            
            var dr1 = os.graphics.Managers.Entity.Create();
            dr1.Graphics.Add.Mesh("doctor");
            dr1.Graphics.Add.Texture("people");
            dr1.AI = os.ai.Create.Entity();
            dr1.FSM = os.ai.Create.FSM(dr1);
            dr1.path = [];
            ED.Entities.doctors[1] = dr1;
            
            var dr2 = os.graphics.Managers.Entity.Create();
            dr2.Graphics.Add.Mesh("doctor");
            dr2.Graphics.Add.Texture("people");
            dr2.AI = os.ai.Create.Entity();
            dr2.FSM = os.ai.Create.FSM(dr2);
            dr2.path = [];
            ED.Entities.doctors[2] = dr2;
            
            //mat4.rotateX(ED.Entities.dr.Default.Offset, degToRad(90), ED.Entities.dr.Default.Offset);
            //
            //  Nurses
            //
            
            //FT Nurse
            ED.Entities.nurse = os.graphics.Managers.Entity.Create();
            ED.Entities.nurse.Graphics.Add.Mesh("nurse");
            ED.Entities.nurse.Graphics.Add.Texture("people");
            ED.Entities.nurse.AI = os.ai.Create.Entity();
            ED.Entities.nurse.FSM = os.ai.Create.FSM(ED.Entities.nurse);
            ED.Entities.nurse.path = [];
            ED.Entities.nurses[0] = ED.Entities.nurse;
            ED.Entities.nurse.home = [35,0,25.3];
            ED.Entities.nurse.triage = "FT";
            ED.Entities.nurse.location = "FTHome";
            ED.Entities.nurse.FSM.Transition("NSIdle");
            
            //Urgent Nurse
            var nurse1 = os.graphics.Managers.Entity.Create();
            nurse1.Graphics.Add.Mesh("nurse");
            nurse1.Graphics.Add.Texture("people");
            nurse1.AI = os.ai.Create.Entity();
            nurse1.FSM = os.ai.Create.FSM(nurse1);
            nurse1.path = [];
            nurse1.home = [-35,0,25.3];
            nurse1.triage = "Urgent";
            nurse1.FSM.Transition("NSIdle");
            nurse1.location = "UrgentHome";
            ED.Entities.nurses[1] = nurse1;
            
            //RNT Nurse
            var nurse2 = os.graphics.Managers.Entity.Create();
            nurse2.Graphics.Add.Mesh("nurse");
            nurse2.Graphics.Add.Texture("people");
            nurse2.AI = os.ai.Create.Entity();
            nurse2.FSM = os.ai.Create.FSM(nurse2);
            nurse2.path = [];
            nurse2.home = [-15,0,-27];
            nurse2.triage = "RNT";
            nurse2.FSM.Transition("NSIdle");
            nurse2.location = "Home";
            ED.Entities.nurses[2] = nurse2;
            
            //RNT Nurse
            var nurse3 = os.graphics.Managers.Entity.Create();
            nurse3.Graphics.Add.Mesh("nurse");
            nurse3.Graphics.Add.Texture("people");
            nurse3.AI = os.ai.Create.Entity();
            nurse3.FSM = os.ai.Create.FSM(nurse3);
            nurse3.path = [];
            nurse3.home = [3,0,-27];
            nurse3.FSM.Transition("NSIdle");
            nurse3.location = "Home";
            nurse3.triage = "RNT"
            ED.Entities.nurses[3] = nurse3;
            
            
            ED.Entities.patient = os.graphics.Managers.Entity.Create();
            ED.Entities.patient.Graphics.Add.Mesh("patient");
            ED.Entities.patient.Graphics.Add.Texture("people");
            ED.Entities.patient.AI = os.ai.Create.Entity();
            ED.Entities.patient.FSM = os.ai.Create.FSM(ED.Entities.patient);
            
            ED.Entities.ed = os.graphics.Managers.Entity.Create();
            ED.Entities.ed.Graphics.Add.Mesh("ed");
            ED.Entities.ed.Graphics.Add.Texture("ed");
            ED.Entities.ed.Move.Up(2);
            //mat4.rotateX(ED.Entities.ed.Default.Offset, degToRad(90), ED.Entities.ed.Default.Offset);
            
        },
        States: function(){
            //
            //  Doctor States
            //
                //Idle
                var DSIdle = os.ai.Create.State("DSIdle");
                DSIdle.Enter = function(self){
                    vec3.set(self.home, self.Position);
                }
                DSIdle.Execute = function(self, dt){
                    if(self.triage == "RNT"){   //REnT
                        if(ED.PendingOrders.RNT.doctor.length > 0){
                            var order = ED.PendingOrders.RNT.doctor.shift();
                            self.FSM.Transition(order.name, order);
                        }
                    }
                    else{                       //Urgent
                        if(ED.PendingOrders.Urgent.doctor.length > 0){
                            var order = ED.PendingOrders.Urgent.doctor.shift();
                            self.FSM.Transition(order.name, order);
                        }
                    }
                }
                //Asses
                var DSAssess = os.ai.Create.State("DSAssess");
                DSAssess.Enter = function(self,order){
                    self.path = ED.Map.GetPath(self.location, order.room.name);
                    self.order = order;
                    self.order.timeElapsed = 0;
                }
                
                DSAssess.Execute = function(self,dt){
                    if(self.path.length == 0 &&!self.AI.MovementAlgorithms.ArrivePosition){
                        if(self.order.timeElapsed == 0){
                            self.order.timeElapsed = dt;
                            self.Move.Backward(5);
                            self.location = self.order.room.name;
                        }
                        else{
                            self.order.timeElapsed += dt;
                            
                            if(self.order.timeElapsed > 5000){
                                if(self.triage == "Urgent"){
                                    //Create Lab Order for Nurse
                                    var order = new COrder("NSLabs");
                                    order.room = self.order.room;
                                    self.ordersPlaced = order.time;
                                    order.patient = self.order.patient;
                                    ED.PendingOrders.Urgent.nurse.push(order);
                                    
                                    if(ED.PendingOrders.Urgent.doctor.length > 0){
                                        self.order = ED.PendingOrders.Urgent.doctor.shift();
                                        self.FSM.Transition(self.order.name, self.order);
                                    }
                                    else{
                                        self.location = "UrgentNurseStation";
                                        self.FSM.Transition("DSIdle");
                                        
                                    }
                                }
                                else{       //RNT
                                     //Create Lab Order for Nurse
                                    var order = new COrder("NSLabs");
                                    order.room = self.order.room;
                                    self.ordersPlaced = order.time;
                                    order.patient = self.order.patient;
                                    ED.PendingOrders.RNT.nurse.push(order);
                                    
                                    if(ED.PendingOrders.RNT.doctor.length > 0){
                                        self.order = ED.PendingOrders.RNT.doctor.shift();
                                        self.FSM.Transition(self.order.name, self.order);
                                    }
                                    else{
                                        self.location = "RNTNurseStation";
                                        self.FSM.Transition("DSIdle");
                                        
                                    }
                                }
                                
                            }
                            
                        }
                    }
                }
                
                //Consult
                var DSConsult = os.ai.Create.State("DSConsult");
                DSConsult.Enter = function(self,order){
                    self.path = ED.Map.GetPath(self.location, order.room.name);
                    self.order = order;
                    self.order.timeElapsed = 0;
                }
                
                DSConsult.Execute = function(self,dt){
                    if(self.path.length == 0 &&!self.AI.MovementAlgorithms.ArrivePosition){
                        if(self.order.timeElapsed == 0){
                            self.order.timeElapsed = dt;
                            self.Move.Backward(10);
                            self.location = self.order.room.name;
                        }
                        else{
                            self.order.timeElapsed += dt;
                            
                            if(self.order.timeElapsed > 20000){
                                if(self.triage == "Urgent"){
                                    //Create Discharge Order for Doctor
                                    var order = new COrder("DSDischarge");
                                    order.room = self.order.room;
                                    self.ordersPlaced = order.time;
                                    order.patient = self.order.patient;
                                    ED.PendingOrders.Urgent.doctor.push(order);
                                    
                                    if(ED.PendingOrders.Urgent.doctor.length > 0){
                                        self.order = ED.PendingOrders.Urgent.doctor.shift();
                                        self.FSM.Transition(self.order.name, dt);
                                    }
                                    else{
                                        self.location = "UrgentNurseStation";
                                        self.FSM.Transition("DSIdle");
                                        
                                    }
                                }
                                else{       //RNT
                                     //Create Discharge Order for Doctor
                                    var order = new COrder("DSDischarge");
                                    order.room = self.order.room;
                                    self.ordersPlaced = order.time;
                                    order.patient = self.order.patient;
                                    ED.PendingOrders.RNT.doctor.push(order);
                                    
                                    if(ED.PendingOrders.RNT.doctor.length > 0){
                                        self.order = ED.PendingOrders.RNT.doctor.shift();
                                        self.FSM.Transition(self.order.name, dt);
                                    }
                                    else{
                                        self.location = "RNTNurseStation";
                                        self.FSM.Transition("DSIdle");
                                        
                                    }
                                }
                                
                            }
                            
                        }
                    }
                }
                //Dischrage
                var DSDischarge = os.ai.Create.State("DSDischarge");
                DSDischarge.Enter = function(self,order){
                    self.path = ED.Map.GetPath(self.location, self.order.room.name);
                    //self.order = order;
                    self.order.timeElapsed = 0;
                }
                DSDischarge.Execute = function(self,dt){
                    if(self.path.length == 0 &&!self.AI.MovementAlgorithms.ArrivePosition){
                        if(self.order.timeElapsed == 0){
                            self.order.timeElapsed = dt;
                            self.Move.Backward(5);
                            self.location = self.order.room.name;
                        }
                        else{
                            self.order.timeElapsed += dt;
                            
                            if(self.order.timeElapsed > 5000){
                                self.order.patient.room = self.order.room;
                                Math.random() < 0.0 ? self.order.patient.FSM.Transition("PSDischarge", "Home") : self.order.patient.FSM.Transition("PSDischarge", "Home");
                                
                                if(self.triage == "Urgent"){
                                    if(ED.PendingOrders.Urgent.doctor.length > 0){
                                        self.order = ED.PendingOrders.Urgent.doctor.shift();
                                        self.FSM.Transition(self.order.name, self.order);
                                    }
                                    else{
                                        self.location = "UrgentNurseStation";
                                        self.FSM.Transition("DSIdle");
                                        
                                    }
                                }
                                else{       //RNT
                                    
                                    if(ED.PendingOrders.RNT.doctor.length > 0){
                                        self.order = ED.PendingOrders.RNT.doctor.shift();
                                        self.FSM.Transition(self.order.name, self.order);
                                    }
                                    else{
                                        self.location = "RNTNurseStation";
                                        self.FSM.Transition("DSIdle");
                                        
                                    }
                                }
                                
                            }
                            
                        }
                    }
                }
            
            //
            //  Nurse Sates
            //
                //Idle
                var NSIdle = os.ai.Create.State("NSIdle");
                NSIdle.Enter = function(self){
                    vec3.set(self.home, self.Position);
                }
                NSIdle.Execute = function(self, dt){
                    if(self.triage == "FT"){
                        if(ED.PendingOrders.FT.length > 0){
                            var order = ED.PendingOrders.FT.shift();
                            self.FSM.Transition(order.name, order);
                        }
                    }
                    else if(self.triage == "RNT"){
                        if(ED.PendingOrders.RNT.nurse.length > 0){
                            var order = ED.PendingOrders.RNT.nurse.shift();
                            self.FSM.Transition(order.name, order);
                        }
                    }
                    else{
                        if(ED.PendingOrders.Urgent.nurse.length > 0){
                            var order = ED.PendingOrders.Urgent.nurse.shift();
                            self.FSM.Transition(order.name, order);
                        }
                    }
                }
                //EvalAndTreatment
                var NSEvalAndTreat = os.ai.Create.State("NSEvalAndTreat");
                NSEvalAndTreat.Enter = function(self, order){
                    self.path = ED.Map.GetPath(self.location, order.room.name);
                    self.order = order;
                    self.order.timeElapsed = 0;
                }
                NSEvalAndTreat.Execute = function(self, dt){
                    if(self.path.length == 0 &&!self.AI.MovementAlgorithms.ArrivePosition){
                        if(self.order.timeElapsed == 0){
                            self.order.timeElapsed = dt;
                            self.Move.Backward(5);
                            self.location = self.order.room.name;
                        }
                        else{
                            self.order.timeElapsed += dt;
                            
                            if(self.order.timeElapsed > 10000){
                                self.order.patient.FSM.Transition("PSDischarge", "Home");
                                //self.FSM.Transition("Idle");
                                if(ED.PendingOrders.FT.length > 0){
                                    self.order = ED.PendingOrders.FT.shift();
                                    self.FSM.Transition(self.order.name, self.order);
                                }
                                else{
                                    self.location = "FTHome";
                                    self.FSM.Transition("NSIdle");
                                    
                                }
                            }
                        }
                    }
                }
                //Vitals
                var NSVitals = os.ai.Create.State("NSVitals");
                NSVitals.Enter = function(self,order){
                    self.path = ED.Map.GetPath(self.location, order.room.name);
                    self.order = order;
                    self.order.timeElapsed = 0;
                }
                NSVitals.Execute = function(self,dt){
                    if(self.path.length == 0 &&!self.AI.MovementAlgorithms.ArrivePosition){
                        if(self.order.timeElapsed == 0){
                            self.order.timeElapsed = dt;
                            self.Move.Backward(5);
                            self.location = self.order.room.name;
                        }
                        else{
                            self.order.timeElapsed += dt;
                            
                            if(self.order.timeElapsed > 5000){
                                if(self.triage == "Urgent"){
                                    //Create Assess Order for Doctor
                                    var order = new COrder("DSAssess");
                                    order.room = self.order.room;
                                    self.ordersPlaced = order.time;
                                    order.patient = self.order.patient;
                                    ED.PendingOrders.Urgent.doctor.push(order);
                                    
                                    if(ED.PendingOrders.Urgent.nurse.length > 0){
                                        self.order = ED.PendingOrders.Urgent.nurse.shift();
                                        self.FSM.Transition(self.order.name, self.order);
                                    }
                                    else{
                                        self.location = "UrgentHome";
                                        self.FSM.Transition("NSIdle");
                                        
                                    }
                                }
                                else{       //RNT
                                    //Create Assess Order for Doctor
                                    var order = new COrder("DSAssess");
                                    order.room = self.order.room;
                                    self.ordersPlaced = order.time;
                                    order.patient = self.order.patient;
                                    ED.PendingOrders.RNT.doctor.push(order);
                                    
                                    if(ED.PendingOrders.RNT.nurse.length > 0){
                                        self.order = ED.PendingOrders.RNT.nurse.shift();
                                        self.FSM.Transition(self.order.name, self.order);
                                    }
                                    else{
                                        self.location = "RNTHome";
                                        self.FSM.Transition("NSIdle");
                                        
                                    }
                                }
                                
                            }
                            
                        }
                    }
                }
                
                //Labs
                var NSLabs = os.ai.Create.State("NSLabs");
                NSLabs.Enter = function(self, order){
                    self.path = ED.Map.GetPath(self.location, order.room.name);
                    self.order = order;
                    self.order.timeElapsed = 0;
                }
                NSLabs.Execute = function(self,dt){
                    if(self.path.length == 0 &&!self.AI.MovementAlgorithms.ArrivePosition){
                        if(self.order.timeElapsed == 0){
                            self.order.timeElapsed = dt;
                            self.Move.Backward(5);
                            self.location = self.order.room.name;
                        }
                        else{
                            self.order.timeElapsed += dt;
                            
                            if(self.order.timeElapsed > 10000){
                                if(self.triage == "Urgent"){
                                    //Create Consult Order for Doctor
                                    var order = new COrder("DSConsult");
                                    order.room = self.order.room;
                                    self.ordersPlaced = order.time;
                                    order.patient = self.order.patient;
                                    ED.PendingOrders.Urgent.doctor.push(order);
                                    
                                    if(ED.PendingOrders.Urgent.nurse.length > 0){
                                        self.order = ED.PendingOrders.Urgent.nurse.shift();
                                        self.FSM.Transition(self.order.name, self.order);
                                    }
                                    else{
                                        self.location = "UrgentHome";
                                        self.FSM.Transition("NSIdle");
                                        
                                    }
                                }
                                else{       //RNT
                                    //Create Consult Order for Doctor
                                    var order = new COrder("DSConsult");
                                    order.room = self.order.room;
                                    self.ordersPlaced = order.time;
                                    order.patient = self.order.patient;
                                    ED.PendingOrders.RNT.doctor.push(order);
                                    
                                    if(ED.PendingOrders.RNT.nurse.length > 0){
                                        self.order = ED.PendingOrders.RNT.nurse.shift();
                                        self.FSM.Transition(self.order.name, self.order);
                                    }
                                    else{
                                        self.location = "RNTHome";
                                        self.FSM.Transition("NSIdle");
                                        
                                    }
                                }
                                
                            }
                            
                        }
                    }
                }
                
                
            
            //
            //  Patient States
            //
                //Arrive
                var PSArrive = os.ai.Create.State("PSArrive");
                PSArrive.Enter = function(self){
                    //Move to Check In Desk
                    self.path = ED.Map.GetPath("Entrance", "CheckIn");
                    os.ai.Movement.Add.Arrive.Position(self, (self.path.pop()).Position);
                }
                
                PSArrive.Execute = function(self, dt){
                    //Once Patient Has Arrived at Check In Desk
                    // Determine Triage and Admit
                    if(self.path.length == 0 && !self.AI.MovementAlgorithms.ArrivePosition){
                        var rand = Math.random();
                        if(rand < 0.2){         //Admit To Fast Track
                            self.FSM.Transition("PSAdmit", "FT");
                        }
                        else if(rand < 0.4){    //Admit To Urgent Care
                            self.FSM.Transition("PSAdmit", "Urgent");
                        }
                        else{                   //Admit to REnT
                            self.FSM.Transition("PSAdmit", "REnT");
                        }
                    }
                }
                
                PSArrive.Exit = function(self){
                    //Set orders placed time to default (0)
                    self.ordersPlaced = 0;
                }
                
                //Admit
                var PSAdmit = os.ai.Create.State("PSAdmit");
                PSAdmit.Enter = function(self, triage){
                    var room = false;
                    self.triage = triage;

                    if(triage == "FT"){             //Admit to FT
                        
                        //Find an open room
                        for(var i = ED.Resources.Rooms.FT.length - 1; i >= 0 && !room; i--){
                            room = ED.Resources.Rooms.FT[i].occupied ? false : ED.Resources.Rooms.FT[i];
                        }
                        
                        
                        if(room){   //Get Path to Room
                            room.occupied = true;
                            self.path = ED.Map.GetPath("CheckIn", room.name);
                        }
                        else{       //Move to Waiting State
                            self.FSM.Transition("PSWaiting", "FT");
                        }
                        self.room = room;
                    }
                    else if(triage == "REnT"){      //Admit to REnT

                        //Find an open room
                        for(var i = ED.Resources.Rooms.RNT.length - 1; i >= 0 && !room; i--){
                            room = ED.Resources.Rooms.RNT[i].occupied ? false : ED.Resources.Rooms.RNT[i];
                        }
                        
                        
                        if(room){   //Get Path to Room
                            room.occupied = true;
                            self.path = ED.Map.GetPath("CheckIn", room.name);
                        }
                        else{       //Move to Waiting State
                            self.FSM.Transition("PSWaiting", "REnT");
                        }
                        
                        self.room = room;
                    }
                    else{                           //Admit to Urgent Care
                        //Find an open room
                        for(var i = ED.Resources.Rooms.Urgent.length - 1; i >= 0 && !room; i--){
                            room = ED.Resources.Rooms.Urgent[i].occupied ? false : ED.Resources.Rooms.Urgent[i];
                        }
                        
                        
                        if(room){   //Get Path to Room
                            room.occupied = true;
                            self.path = ED.Map.GetPath("CheckIn", room.name);
                        }
                        else{       //Move to Waiting State
                            self.FSM.Transition("PSWaiting", "Urgent");
                        }
                        
                        self.room = room;
                    }
                    
                    
                }
                
                PSAdmit.Execute = function(self, dt){
                    
                    //Once Arrive at Room, Alert someone
                    if(self.path.length == 0 &&!self.AI.MovementAlgorithms.ArrivePosition){
                        if(self.ordersPlaced == 0){
                            if(self.triage == "FT"){
                                var order = new COrder("NSEvalAndTreat");
                                order.room = self.room;
                                self.ordersPlaced = order.time;
                                order.patient = self;
                                ED.PendingOrders.FT.push(order);
                                self.yaw += 180;
                            }
                            else if(self.triage == "REnT"){
                                var order = new COrder("NSVitals");
                                order.room = self.room;
                                self.ordersPlaced = order.time;
                                order.patient = self;
                                ED.PendingOrders.RNT.nurse.push(order);
                                self.yaw += 180;
                            }
                            else{
                                var order = new COrder("NSVitals");
                                order.room = self.room;
                                self.ordersPlaced = order.time;
                                order.patient = self;
                                ED.PendingOrders.Urgent.nurse.push(order);
                                self.yaw += 180;
                            }
                            
                            
                        }
                        
                    }
                }
                PSAdmit.Exit = function(self){
                    
                }
                //Waiting
                var PSWaiting = os.ai.Create.State("PSWaiting");
                PSWaiting.Enter = function(self, triage){
                    if(triage == "FT"){             //Waiting on FT
                        //Wait Queue position (-22, 0, 72);
                        self.Position[0] = -22;
                        self.Position[1] = 7.2 * ED.Resources.Rooms.Waiting.FT.length;
                        self.Position[2] = 72;
                        self.location = "FTWaiting";
                        ED.Resources.Rooms.Waiting.FT.push(self);
                        
                    }
                    else if(triage == "REnT"){      //Waiting REnT
                        //Wait Queue position (-22, 0, 72);
                        self.Position[0] = -22;
                        self.Position[1] = 7.2 * ED.Resources.Rooms.Waiting.RNT.length;
                        self.Position[2] = 82;
                        self.location = "RNTWaiting";
                        ED.Resources.Rooms.Waiting.RNT.push(self);
                    }
                    else{                           //Waiting Urgent Care
                        //Wait Queue position (-22, 0, 72);
                        self.Position[0] = -22;
                        self.Position[1] = 7.2 * ED.Resources.Rooms.Waiting.Urgent.length;
                        self.Position[2] = 92;
                        self.location = "UrgentWaiting";
                        ED.Resources.Rooms.Waiting.Urgent.push(self);
                    }
                }
                PSWaiting.Execute = function(self, dt){
                    if(self.path.length == 0 && !self.AI.MovementAlgorithms.ArrivePosition){
                        if(self.location == "CheckIn"){
                            self.FSM.Transition("PSAdmit", self.triage);
                        }
                    }
                }
                
                //Discharge
                var PSDischarge = os.ai.Create.State("PSDischarge");
                PSDischarge.Enter = function(self, loc){
                    if(loc == "Home"){  //Sending Home
                        self.path = ED.Map.GetPath(self.room.name, "Start");
                    }
                    else{               //Admiting to Hospital
                        self.path = ED.Map.GetPath(self.room.name, "Hospital");
                    }
                    self.room.occupied = false;
                    
                    //Check for people waiting for room
                    if(self.triage == "FT"){
                        if(ED.Resources.Rooms.Waiting.FT.length > 0){
                            var patient = ED.Resources.Rooms.Waiting.FT.shift();
                            patient.path = ED.Map.GetPath("FTWaiting", "CheckIn");
                            patient.location = "CheckIn";
                            //Update Heights of all remaining in Queue
                            for(var i = 0; i < ED.Resources.Rooms.Waiting.FT.length; i++){
                                ED.Resources.Rooms.Waiting.FT[i].Position[1] = 7.2 * i;
                            }
                        }
                    }
                    else if(self.triage == "REnT"){
                        if(ED.Resources.Rooms.Waiting.RNT.length > 0){
                            var patient = ED.Resources.Rooms.Waiting.RNT.shift();
                            patient.path = ED.Map.GetPath("RNTWaiting", "CheckIn");
                            patient.location = "CheckIn";
                            //Update Heights of all remaining in Queue
                            for(var i = 0; i < ED.Resources.Rooms.Waiting.RNT.length; i++){
                                ED.Resources.Rooms.Waiting.RNT[i].Position[1] = 7.2 * i;
                            }
                        }
                    }
                    else{
                        if(ED.Resources.Rooms.Waiting.Urgent.length > 0){
                            var patient = ED.Resources.Rooms.Waiting.Urgent.shift();
                            patient.path = ED.Map.GetPath("UrgentWaiting", "CheckIn");
                            patient.location = "CheckIn";
                            //Update Heights of all remaining in Queue
                            for(var i = 0; i < ED.Resources.Rooms.Waiting.Urgent.length; i++){
                                ED.Resources.Rooms.Waiting.Urgent[i].Position[1] = 7.2 * i;
                            }
                        }
                    }
                }
                PSDischarge.Execute = function(self,dt){
                    if(self.path.length == 0 && !self.AI.MovementAlgorithms.ArrivePosition){
                        
                        for(var i = ED.Entities.patients.length - 1; i >= 0 && self; i-- ){
                            if(ED.Entities.patients[i].Get.id() == self.Get.id()){
                                ED.Entities.patients.splice(i,1);
                                self = null;
                            }
                        }
                    }
                }
                
        },
        Resources: function(){
            
            //Fast Track Rooms
            for(var i = 0; i < 21; i++){
                ED.Resources.Rooms.FT[i] = new CRoom("FT"+(i+1));
            }
            
            //Urgent Rooms
            for(var i = 0; i < 21; i++){
                ED.Resources.Rooms.Urgent[i] = new CRoom("Urgent"+(i+1));
            }
            
        },
        Controls: function(){
            
        },
        Menu: function(){
            
        },
        Map: function(){
            ED.Map.Nodes = os.resschmgr.Create.Map();
            ED.Map.Astar.activePaths = os.resschmgr.Create.Map();
            ED.Map.Astar.visitedNodes = os.resschmgr.Create.Map();
            //Build Nodes
            ED.Map.Load("data/NodeMap.csv");
            
        },
        AI: function(){          
            var CAIEntity = function(){
                var self = this;
                this.MovementAlgorithms = {
                    SeekEntity: false,
                    SeekPosition: false,
                    FleeEntity: false,
                    FleePosition: false,
                    ArriveEntity: false,
                    ArrivePosition: false,
                    PatrolCircle: false,
                    HideEntity: false,
                    Flock: false,
                    Cohesion: false,
                    Alignment: false,
                    Seperation: false,
                    Avoidance: false
                }
                
                this.Movement ={
                    Disable: {
                        Seek: {
                            Entity: function(){
                                self.MovementAlgorithms.SeekEntity = false;
                            },
                            Position: function(){
                                self.MovementAlgorithms.SeekPosition = false;
                            }
                        },
                        Hide: {
                            Entity: function(){
                                self.MovementAlgorithms.HideEntity = false;
                            }
                        },
                        Arrive: {
                            Entity: function(){
                                self.MovementAlgorithms.ArriveEntity = false;
                            },
                            Position: function(){
                                self.MovementAlgorithms.ArrivePosition = false;
                            }
                        },
                        Flee: {
                            Entity: function(){
                                self.MovementAlgorithms.ArriveEntity = false;
                            },
                            Position: function(){
                                self.MovementAlgorithms.ArrivePosition = false;
                            }
                        },
                        Patrol: {
                            Circle: function(){
                                self.MovementAlgorithms.PatrolCircle = false;
                            }
                        },
                        Flock: function(){
                            self.MovementAlgorithms.Flock = false;
                        },
                        Cohesion: function(){
                            self.MovementAlgorithms.Cohesion = false
                        },
                        Alignment: function(){
                            self.MovementAlgorithms.Alignment = false;
                        },
                        Seperation: function(){
                            self.MovementAlgorithms.Seperation = false;
                        }
                    },
                    maxSpeed: 0.01,
                    maxForce: 1.9
                }
            }
            
            var CSeekPosition = function(oEntity, vTarget){
                
                var self = this;
                this.entity = oEntity;
                this.Update = function(dt){
                    var desired = [];
                    var applied = [];
                    var sign = [];
                    
                    //Desired Velocity
                    vec3.subtract(self.target,self.entity.Position, desired);
                    vec3.normalize(desired,desired);
                    
                    
                    //**********************
                    //   Force Calculation
                    //
                    //**********************
                    
                    vec3.scale(desired,self.entity.AI.Movement.maxSpeed,desired);
                    
                    //Applied Velocity
                    vec3.subtract(desired,self.entity.Axis.Look, applied);
                    
                    //Scale Final Applied Force
                    if(vec3.length(applied) > self.entity.AI.Movement.maxForce){
                        vec3.normalize(applied, applied);
                        vec3.scale(applied, self.entity.AI.Movement.maxForce);
                    }
                    
                    //Calculate new Look Vector
                    vec3.add(self.entity.Axis.Look, applied, desired);
                    vec3.normalize(desired, desired);
                    
                    //**********************
                    //   Adjust rotation
                    //
                    //**********************
                    
                    //Calculate sign for rotation (Y parameter)
                    vec3.cross(self.entity.Axis.Look, desired, sign);
                    
                    //Calculate angle of rotation
                    var angle = Math.acos((vec3.dot(self.entity.Axis.Look,desired)/vec3.length(desired)).toFixed(5)) * 180 / Math.PI;
                    
                    //Adjust to negative angle if sign is positive
                    self.entity.yaw = sign[1] < 0 ? self.entity.yaw + (angle * -1) : self.entity.yaw + angle;
                    
                    //**********************
                    //   Adjust Position
                    //
                    //**********************
                    vec3.scale(desired, self.entity.AI.Movement.maxSpeed * dt);
                    vec3.add(self.entity.Position, desired, self.entity.Position);
                    
                }
                
                this.target = vTarget;
                
                
            }
            var CSeekEntity = function(oEntity, oTarget){
                var self = this;
                this.entity = oEntity;
                this.Update = function(){
                    var t = [];
                    t = self.target.Physics ? self.target.Position : self.target.Position;
                    var desired = [];
                    var applied = [];
                    
                    //Desired Velocity
                    vec3.subtract(t,self.entity.Position, desired);
                    vec3.normalize(desired,desired);
                    
                    //Adjust rotation
                    //Find axis of rotation 
                    var rot = [];
                    
                    vec3.cross([0,0,-1], desired, rot);
                
                    var angle = Math.acos(vec3.dot(desired, [0,0,1]));//Math.asin(vec3.length(rot));
                    
                    vec3.normalize(rot,rot);
                    
                    //If greater than 5 degrees
                    //if(angle > 0.087266)
                        var cos = Math.cos(angle/2);
                        var sin = Math.sin(angle/2);
    
                        var percent = 1;
                        var quat = [rot[0]*sin, rot[1]*sin, rot[2]*sin, cos];
                        
                        self.entity.Physics.orientation = [rot[0]*sin, rot[1]*sin, rot[2]*sin, cos];
                        
                        //quat4.slerp(self.entity.Physics.orientation, quat, 1, self.entity.Physics.orientation);
                        //var orientInv = [];
                        //quat4.inverse(self.entity.Physics.orientation, orientInv);
                        //
                        //quat4.multiply(orientInv, quat, quat);
                        //
                        //quat[0] = Math.pow(quat[0], percent);
                        //quat[1] = Math.pow(quat[1], percent);
                        //quat[2] = Math.pow(quat[2], percent);
                        //quat[3] = Math.pow(quat[3], percent);
                        //
                        //quat4.normalize(quat,quat);
                        //
                        //quat4.multiply(self.entity.Physics.orientation,quat ,self.entity.Physics.orientation);
    
                    //
                    //else
                    //                                            //greater than 90                   less than 90
                    //    vec3.dot([0,0,-1], desired) <  0 ? self.entity.Physics.orientation : quat4.inverse(self.entity.Physics.orientation);
                    //    
                    //
                    
                    vec3.scale(desired,self.entity.AI.Movement.maxForce,desired);
                    
                    //Applied Velocity
                    vec3.subtract(desired,self.entity.Axis.Look, applied);
                    
                    //Scale Final Applied Force
                    if(vec3.length(applied) > self.entity.AI.Movement.maxForce){
                        vec3.normalize(applied, applied);
                        vec3.scale(applied, self.entity.AI.Movement.maxForce);
                    }
                    
                    //Apply Force
                    os.physics.Create.Force.Impulse(self.entity.Physics,[0,0,0],applied);
                    
                    
                    
                }
                
                this.target = oTarget;
            }
            
            var CFleePosition = function(){
                
            }
            var CFleeEntity = function(oEntity, oTarget){
                var self = this;
                this.entity = oEntity;
                this.Update = function(){
                    var t = [];
                    t = self.target.Physics ? self.target.Position : self.target.Position;
                    var desired = [];
                    var applied = [];
                    
                    //Desired Velocity
                    vec3.subtract(self.entity.Position,t, desired);
                    vec3.normalize(desired,desired);
                    
                    //Adjust rotation
                    //Find axis of rotation 
                    var rot = [];
                    
                    vec3.cross([0,0,-1], desired, rot);
                
                    var angle = Math.acos(vec3.dot(desired, [0,0,1]));//Math.asin(vec3.length(rot));
                    
                    vec3.normalize(rot,rot);
                    
                    var cos = Math.cos(angle/2);
                    var sin = Math.sin(angle/2);
    
                    var percent = 1;
                    var quat = [rot[0]*sin, rot[1]*sin, rot[2]*sin, cos];
                    
                    self.entity.Physics.orientation = [rot[0]*sin, rot[1]*sin, rot[2]*sin, cos];
                        
                    
                    vec3.scale(desired,self.entity.AI.Movement.maxForce,desired);
                    
                    //Applied Velocity
                    vec3.subtract(desired,self.entity.Axis.Look, applied);
                    
                    //Scale Final Applied Force
                    if(vec3.length(applied) > self.entity.AI.Movement.maxForce){
                        vec3.normalize(applied, applied);
                        vec3.scale(applied, self.entity.AI.Movement.maxForce);
                    }
                    
                    //Apply Force
                    os.physics.Create.Force.Impulse(self.entity.Physics,[0,0,0],applied);
                    
                    
                    
                }
                
                this.target = oTarget;
            }
            var CFleeEntityBound = function(){
                
            }
            
            var CArrivePosition = function(oEntity, vTarget){
                 var self = this;
                this.entity = oEntity;
                this.Update = function(){
                    var expired = false;
                    var dt = 16;
                    var desired = [];
                    var distance = [];
                    var applied = [];
                    var sign = [];
                    
                    //Desired Velocity
                    vec3.subtract(self.target,self.entity.Position, distance);
                    vec3.normalize(distance,desired);
                    distance = vec3.length(distance);
                    
                    
                    //**********************
                    //   Force Calculation
                    //
                    //**********************
                    
                    vec3.scale(desired,self.entity.AI.Movement.maxSpeed,desired);
                    
                    //Applied Velocity
                    vec3.subtract(desired,self.entity.Axis.Look, applied);
                    
                    //Scale Final Applied Force
                    if(vec3.length(applied) > self.entity.AI.Movement.maxForce){
                        vec3.normalize(applied, applied);
                        vec3.scale(applied, self.entity.AI.Movement.maxForce);
                    }

                    //Scale Final Applied Force
                    if(distance < (self.offsetDistance)){
                        
                        //Scale Final Applied Force
                        if(vec3.length(applied) > self.entity.AI.Movement.maxForce/2){
                            vec3.normalize(applied, applied);
                            vec3.scale(applied, self.entity.AI.Movement.maxForce/2, applied);
                        }
                        
                    }else{
                        //Scale Final Applied Force
                        if(vec3.length(applied) > self.entity.AI.Movement.maxForce){
                            vec3.normalize(applied, applied);
                            vec3.scale(applied, self.entity.AI.Movement.maxForce, applied);
                        }
                    }
                    
                    //Calculate new Look Vector
                    vec3.add(self.entity.Axis.Look, applied, desired);
                    vec3.normalize(desired, desired);
                    
                    //**********************
                    //   Adjust rotation
                    //
                    //**********************
                    
                    //Calculate sign for rotation (Y parameter)
                    vec3.cross(self.entity.Axis.Look, desired, sign);
                    
                    //Calculate angle of rotation
                    var angle = Math.acos((vec3.dot(self.entity.Axis.Look,desired)/vec3.length(desired)).toFixed(5)) * 180 / Math.PI;
                    
                    //Adjust to negative angle if sign is positive
                    self.entity.yaw = sign[1] < 0 ? self.entity.yaw + (angle * -1) : self.entity.yaw + angle;
                    
                    //**********************
                    //   Adjust Position
                    //
                    //**********************
                    
                    
                    if(distance > self.offsetDistance/2){
                        
                        vec3.scale(desired, self.entity.AI.Movement.maxSpeed * dt);
                        vec3.add(self.entity.Position, desired, self.entity.Position);
                        self.lastDistance = distance;
                    }
                    else{// if(distance > self.offsetDistance/2){
                        self.slowing = true;
                        vec3.scale(desired, self.entity.AI.Movement.maxSpeed * dt);
                        vec3.add(self.entity.Position, desired, self.entity.Position);
                    }
                    
                    if(self.slowing && (self.lastDistance < distance)){
                        self.entity.AI.MovementAlgorithms.ArrivePosition = false;
                        expired = true;
                    }
                        
                    self.lastDistance = distance;
                    return expired;
                    
                }
                
                this.target = vTarget;
                this.offsetDistance = 5;
                this.slowing = false;
                this.lastDistance = 0;
            }
            
            var CArriveEntity = function(){
                
            }
            var CHideEntity = function(oEntity, oTarget, vHidingPositions, vOffsets){
                var self = this;
                this.entity = oEntity;
                this.Update = function(){
                    
                    var flee = [];
                    flee = self.target.Physics ? self.target.Position : self.target.Position;
                    
                    var position = [];
                    var distance = 0;
                    var target = [];
                    for(var i = vHidingPositions.length - 1; i >= 0; i--){
                        
                        // HidingPosition - Flee
                        var direction = [];
                        vec3.subtract(vHidingPositions[i], flee, direction);
                        
                        // Distance = Mag(HidingPosition - Flee)
                        var dist = vec3.length(direction);
                        
                        // Normalize (HidingPosition - Flee)
                        vec3.normalize(direction, direction);
                        
                        // Scale(Normalize(HidingPosition - Flee), distance + offset);
                        vec3.scale(direction, vOffsets[i] + dist, position);
                        
                        // Flee + Distance
                        vec3.add(position, flee, position);
                        
                        //Get Distance to offset
                        vec3.subtract(position, self.entity.Position, direction);
                        
                        if(distance == 0){distance = vec3.length(direction)}
                        
                        //Find cloesest point to target 
                        if(vec3.length(direction) < distance){
                            
                            vec3.set(position, target);
                        }
                    
                    }
                    
                    var desired = [];
                    var applied = [];
                    
                    
                    //Desired Velocity
                    vec3.subtract(target,self.entity.Position, desired);
                    vec3.normalize(desired,desired);
                    
                    //Adjust rotation
                    //Find axis of rotation 
                    var rot = [];
                    
                    vec3.cross([0,0,-1], desired, rot);
                
                    var angle = Math.acos(vec3.dot(desired, [0,0,1]));//Math.asin(vec3.length(rot));
                    
                    vec3.normalize(rot,rot);
    
                    var cos = Math.cos(angle/2);
                    var sin = Math.sin(angle/2);
    
                    var percent = 1;
                    var quat = [rot[0]*sin, rot[1]*sin, rot[2]*sin, cos];
                    
                    self.entity.Physics.orientation = [rot[0]*sin, rot[1]*sin, rot[2]*sin, cos];
                        
    
                    
                    vec3.scale(desired,self.entity.AI.Movement.maxForce,desired);
                    
                    //Applied Velocity
                    vec3.subtract(desired,self.entity.Axis.Look, applied);
                    
                    //Scale Final Applied Force
                    if(vec3.length(applied) > self.entity.AI.Movement.maxForce){
                        vec3.normalize(applied, applied);
                        vec3.scale(applied, self.entity.AI.Movement.maxForce);
                    }
                    
                    //Apply Force
                    os.physics.Create.Force.Impulse(self.entity.Physics,[0,0,0],applied);
                    
                    
                    
                }
                
                this.target = oTarget;
            }
            var CPatrolCircle = function(oEntity, fRadius, vCenter){
                var self = this;
                this.entity = oEntity;
                this.radius = fRadius;
                this.center = vCenter;
                this.Update = function(){
                    var position  = [];
                    
                    //Get predictd next position
                    var VoDt = [];
                    vec3.scale(self.entity.Physics.velocity, 1.05, VoDt);
                    vec3.add(self.entity.Position, VoDt, position);
                    
                    //Map predicted position to sphere
                    //  Get direction
                    vec3.subtract(position, self.center, position);
                    vec3.normalize(position, position);
                    //Scale by radius
                    vec3.scale(position, self.radius, position);
                    //Add to center to get current position on path
                    vec3.add(self.center, position, position);
                    
                    var desired = [];
                    var applied = [];
                    
                    //Desired Velocity
                    vec3.subtract(position,self.entity.Position, desired);
                    vec3.normalize(desired,desired);
                    //**********************
                    //   Adjust rotation
                    //
                    //**********************
                    //Find axis of rotation 
                    var rot = [];
                    vec3.cross([0,0,-1], desired, rot);
                    var angle = Math.acos(vec3.dot(desired, [0,0,1]));//Math.asin(vec3.length(rot));
                    
                    vec3.normalize(rot,rot);
                    var cos = Math.cos(angle/2);
                    var sin = Math.sin(angle/2);
    
                    var percent = 1;
                    var quat = [rot[0]*sin, rot[1]*sin, rot[2]*sin, cos];
                    
                    self.entity.Physics.orientation = [rot[0]*sin, rot[1]*sin, rot[2]*sin, cos];
                    
                    //**********************
                    //   LinearForce Calculation
                    //
                    //**********************
                    
                    vec3.scale(desired,self.entity.AI.Movement.maxForce,desired);
                    
                    //Applied Velocity
                    vec3.subtract(desired,self.entity.Axis.Look, applied);
                    
                    //Scale Final Applied Force
                    if(vec3.length(applied) > self.entity.AI.Movement.maxForce){
                        vec3.normalize(applied, applied);
                        vec3.scale(applied, self.entity.AI.Movement.maxForce);
                    }
                    
                    //Apply Force
                    os.physics.Create.Force.Impulse(self.entity.Physics,[0,0,-1],applied);
                    
                }
            }
            
            var CFlock = function(){
                
            }
            
            var CCohesion = function(){
                
            }
            var CAvoidance = function(oEntity, vHalfSizes, vBVs){
                var self = this;
                this.entity = oEntity;
                this.halfSize = vHalfSizes;
                this.obstacles = vBVs;
                this.bv = os.physics.Create.BV.OBB(null,self.halfSize,self.entity.Position);
                this.bv.name = oEntity.ID();
                
                this.Update = function(){
                    for(var i = self.obstacles.length - 1; i >= 0; i--){
                        collision = self.bv.CollisionTest(self.obstacles[i]);
                        
                        if(collision){
                            //Position = OBB Cloesest Point + (radius * normal)
                            var distance = [];
                            vec3.subtract(collision.point.obj1, self.entity.Position, distance);
                            distance = vec3.length(distance);
                            
                            //Collision on left side
                            if(collision.point.obj1[0] < 0){
                                self.entity.Physics.Add.ForceAtLocalPoint([10000,0,0], [0,0,40]);
                            }
                            //Collision on Right Side
                            else{
                                self.entity.Physics.Add.ForceAtLocalPoint([-10000,0,0], [0,0,40]);
                            }
                            
                        }
                        
                    }
                }
            }
            
            //Holds Arrays of Alogorithms to be updated every loop
            //      key: AlogName, value: AlgoName[CMovingAlgorithm...]
            var _MovementAlgorithms = null;//os.resschmgr.Create.Map();
            
            //Arrays of Algorithms to be updated every loop
            //  AlgoName[CAlgoType] --> stored in MovementAlgo map
            var _SeekEntity = [];
            var _SeekPosition = [];
            var _ArrivePosition = [];
            var _ArriveEntity = [];
            var _FleeEntity = [];
            var _PatrolCircle = [];
            var _HideEntity = [];
            var _Avoidance = [];
            
            //  Finite State Machine and States
            //
            var FiniteStateMachine = function(cEntity){
                var owner = cEntity;
                var _CurrentState = new CState();
                var self = this;
                var _Enter = function(oMessage){
                    _CurrentState.Enter(owner, oMessage);
                }
                
                var _Exit = function(oMessage){
                    _CurrentState.Exit(owner, oMessage);
                }
                
                var _Execute = function(oMessage){
                    _CurrentState.Execute(owner, oMessage);
                }
                
                //Control Methods
                this.Transition = function(sName, oMessage){
                    //Call Exit for Current State
                    _Exit(oMessage);
                    
                    //Get New State to Transition Too
                    _CurrentState = _States.get(sName);
                    
                    //Call New States Setup and Exectue Methods
                    _Enter(oMessage);
                    _Execute(oMessage);
                }
                this.SetState = function(sName){
                    _CurrentState = _States.get(sName);
                }
                this.Update = function(oMessage){
                    _Execute(oMessage);
                }
                this.GetState = function(){
                    return _CurrentState.GetName();
                }
            }
            
            var CState = function(sName){
                var _name = sName;
                var self = this;
                
                this.Enter = function(cEntity, oMessage){
                    //Override this Method
                }
                
                this.Exit = function(cEntity, oMessage){
                    //Override this Method
                }
                
                this.Execute = function(cEntity, oMessage){
                    //Override this Method
                }
                
                this.GetName = function(){
                    return _name;
                }
    
            }
            //Holds all states created
            // key: name, value: CState
            var _States = null;//os.resschmgr.Create.Map()
            
            os.ai = {
                Initialize: function(){
                    //Create All Map Objects
                    _States = os.resschmgr.Create.Map();
                    _MovementAlgorithms = os.resschmgr.Create.Map();
                    
                    //Load all algo arrays into movment map
                    _MovementAlgorithms.put("SeekEntity", _SeekEntity);
                    _MovementAlgorithms.put("SeekPosition", _SeekPosition);
                    _MovementAlgorithms.put("ArriveEntity", _ArriveEntity);
                    _MovementAlgorithms.put("ArrivePosition", _ArrivePosition);
                    _MovementAlgorithms.put("FleeEntity", _FleeEntity);
                    _MovementAlgorithms.put("FleeEntity", _FleeEntity);
                    _MovementAlgorithms.put("PatrolCircle", _PatrolCircle);
                    _MovementAlgorithms.put("HideEntity", _HideEntity);
                    _MovementAlgorithms.put("Avoidance", _Avoidance);
                    
                },
                Create: {
                    Entity: function(){
                        var ent = new CAIEntity();
                        return ent;
                    },
                    FSM: function(cEntity){
                        return new FiniteStateMachine(cEntity);
                        
                    },
                    State: function(sName){
                        var state = new CState(sName);
                        _States.put(sName, state);
                        return state;
                    }
                },
                Get: {
                    State: function(sName){
                        return _States.get(sName);
                    }
                },
                Update: function(dt){
                    
                    //Update Movement Algorithms
                    os.ai.Movement.Update(dt);
                    
                },
                Movement: {
                    Add: {
                        Seek: {
                            Position: function(oEntity, vTarget){
                                var algo = new CSeekPosition(oEntity, vTarget);
                                _SeekPosition.push(algo);
                                oEntity.AI.MovementAlgorithms.SeekPosition = true;
                                return algo;
                            },
                            Entity: function(oEntity, oTarget){
                                var algo = new CSeekEntity(oEntity, oTarget);
                                _SeekEntity.push(algo);
                                oEntity.AI.MovementAlgorithms.SeekEntity = true;
                                return algo;
                            }
                        },
                        Flee: {
                            Position: function(){
                                
                            },
                            Entity: function(oEntity, oTarget){
                                var algo = new CFleeEntity(oEntity, oTarget);
                                _FleeEntity.push(algo);
                                oEntity.AI.MovementAlgorithms.FleeEntity = true;
                                return algo;
                            }
                        },
                        Arrive: {
                            Position: function(oEntity, vTarget){
                                var algo = new CArrivePosition(oEntity, vTarget);
                                _ArrivePosition.push(algo);
                                oEntity.AI.MovementAlgorithms.ArrivePosition = true;
                                return algo;
                            },
                            Entity: function(){
                                
                            }
                        },
                        Hide: {
                            Entity: function(oEntity, oTarget, vHidingPositions, vOffsets){
                                var algo = new CHideEntity(oEntity, oTarget, vHidingPositions, vOffsets);
                                _HideEntity.push(algo);
                                oEntity.AI.MovementAlgorithms.HideEntity = true;
                                return algo;
                            }
                        },
                        Patrol: {
                            Circle: function(oEntity, fRadius, vCenter){
                                var algo = new CPatrolCircle(oEntity, fRadius, vCenter);
                                _PatrolCircle.push(algo);
                                oEntity.AI.MovementAlgorithms.PatrolCircle = true;
                                return algo;
                            }
                        },
                        Avoidance: function(oEntity, vHalfSizes, vBVs){
                            var algo = new CAvoidance(oEntity, vHalfSizes, vBVs);
                            _Avoidance.push(algo);
                            oEntity.AI.MovementAlgorithms.Avoidance = true;
                            return algo;
                        }
                        
                    },
                    Update: function(dt){
                        
                        for(var i = _MovementAlgorithms.size; i--; _MovementAlgorithms.next()){
                            //Get Algorithm Array, and Key (Algorithm Type)
                            var algo = _MovementAlgorithms.value();
                            var type = _MovementAlgorithms.key();
                            
                            for(var j = algo.length - 1; j >= 0; j--){
                                
                                //algo[j].entity.AI.MovementAlgorithms[type] ? algo[j].Update(dt) : algo.splice(j,1);
                                algo[j].Update(dt) ? algo.splice(j,1) : false;
                                
                            }
                        }
                    }
                }
            }
            
            os.ai.Initialize();
        }
    
    },
    CreatePatient: function(){
        var patient = os.graphics.Managers.Entity.Create();
        patient.Graphics.Add.Mesh("patient");
        patient.Graphics.Add.Texture("people");
        patient.AI = os.ai.Create.Entity();
        patient.FSM = os.ai.Create.FSM(patient);
        patient.path = [];
        ED.Entities.patients.push(patient);
        
        vec3.set(ED.Map.Nodes.get("Start").Position,patient.Position);
        
        patient.FSM.Transition("PSArrive", Math.random());
        
        return patient;
    },
    Map: {
        Nodes: null,
        rawMap: null,
        Load: function(sFilename){
            var self = this;
            var xhr = os.resschmgr.Create.XHRObject();
            var filepath = sFilename;
                
            xhr.open('GET',filepath,false);
            xhr.onreadystatechange = function()
            {
                if(xhr.readyState==4)
                { //4==DONE
                    if(xhr.status == 200)
                    {
                        ED.Map.rawMap = xhr.responseText;
                        ED.Map.BuildMap();
                    }
                    else
                    {
                        os.windows.Create.ErrorWindow('Load Failure', "Failed to load Node Map: URL: " + filepath + "<br/><br/>Check url and filename");
                    }
                    
                }
                                
            };
                        
            xhr.send();
            
        },
        BuildMap: function(){
            var file = ED.Map.rawMap.split('\n');
            //Remove Headers
            file.shift();
            
            os.console.Comment("Parsing and Building Nav Map");
            
            //Populate Node map
            for(var i = file.length - 1; i >= 0; i--){
                var record = file[i].split(',');
                var id = Number(record[0]);
                var name = record[1];
                var position = record.slice(2,5);
                position = position.map(Number);
                
                var empty = record.indexOf("",5);
                
                var connections = empty == -1 ? record.slice(5) : record.slice(5, empty);
                connections = connections.map(Number);
                
                var node = new CNode(name, id, position,connections)
            }
            
            ED.Map.rawMap = "";
            os.console.Comment("Nav Map Initialization Complete");
        },
        GetPath: function(startID, endID){
            //Clear out Active Paths and Visited Nodes map
            ED.Map.Astar.activePaths.removeAll();
            ED.Map.Astar.visitedNodes.removeAll();
            ED.Map.Astar.Path = null;
            
            //Get refrences to start and end nodes
            ED.Map.Astar.Start = new CTreeNode(-1,ED.Map.Nodes.get(startID));
            ED.Map.Astar.End   = new CTreeNode(-1,ED.Map.Nodes.get(endID));
            
            //Initialize A*
            //  Set Start Node, Add Connections
            ED.Map.Astar.Start.weight = 0;
            ED.Map.Astar.Start.heuristic = ED.Map.Astar.GetDistance(ED.Map.Astar.Start, ED.Map.Astar.End);
            ED.Map.Astar.AddConnections(ED.Map.Astar.Start);
            ED.Map.Astar.visitedNodes.put(ED.Map.Astar.Start.id, ED.Map.Astar.Start);
            var path = [];
            
            if(ED.Map.Astar.Start.id != ED.Map.Astar.End.id){
                //Begin A*
                while(ED.Map.Astar.activePaths.size){
                    ED.Map.Astar.SetBPSF();
                    ED.Map.Astar.AddConnections(ED.Map.Astar.BPSF);
                    ED.Map.Astar.UpdateActivePaths();
                }
                
                //Build an array of Nodes from TreeNodes in Reverse Order
                
                while(ED.Map.Astar.Path.id != ED.Map.Astar.Start.id){
                    path.push(ED.Map.Astar.Path.node);
                    ED.Map.Astar.Path = ED.Map.Astar.visitedNodes.get(ED.Map.Astar.Path.parent);
                }
                path.push(ED.Map.Astar.Path.node);
            }
            //Return Array of Nodes in Reverse Order
            return path;
            
        },
        Astar: {
            activePaths: null,
            visitedNodes: null,
            Path: null,
            Start: null,
            End: null,
            BPSF: null,
            GetDistance: function(start, end){
                var vector = [];
                vec3.subtract(start.node.Position, end.node.Position, vector);
                return Math.sqrt(vec3.dot(vector, vector));
                
            },
            UpdateActivePaths: function(){
                
                //Loop through Active Paths
                for(var i = ED.Map.Astar.activePaths.size -  1; i >=0; i--){
                    
                    //Path converged on final desination
                    if(ED.Map.Astar.activePaths.value().id == ED.Map.Astar.End.id){
                        
                        if(ED.Map.Astar.Path){  //If Path exist, test to see if it shorter
                            if(ED.Map.Astar.activePaths.value().weight < ED.Map.Astar.Path.weight){
                                ED.Map.Astar.Path = ED.Map.Astar.activePaths.value();
                            }
                        }
                        else{                   //No Path exist, set Path to current value
                            ED.Map.Astar.Path = ED.Map.Astar.activePaths.value();
                        }
                        ED.Map.Astar.activePaths.remove(ED.Map.Astar.activePaths.key());
                        if(ED.Map.Astar.activePaths.size > 0){
                            ED.Map.Astar.activePaths.next();
                        }
                    }
                    else{
                        ED.Map.Astar.activePaths.next();
                    }
                    
                }
                
                //If a Path has been found, prune tree of all Active Paths longer that Path
                if(ED.Map.Astar.Path){
                    for(var i = ED.Map.Astar.activePaths.size -  1; i >=0; i--){
                        if((ED.Map.Astar.BPSF.weight + ED.Map.Astar.BPSF.heuristic) < ( (ED.Map.Astar.activePaths.value()).weight + (ED.Map.Astar.activePaths.value()).heuristic )){
                            ED.Map.Astar.activePaths.remove(ED.Map.Astar.activePaths.key());
                        }
                    }
                }
            },
            SetBPSF: function(){
                //Initialize BPSF to a value from map
                ED.Map.Astar.BPSF = ED.Map.Astar.activePaths.value();
                
                //Loop through all Active Paths, and set BPSF to shortest
                for(var i = ED.Map.Astar.activePaths.size - 1; i >=0; i--){
                    if((ED.Map.Astar.BPSF.weight + ED.Map.Astar.BPSF.heuristic) > ( (ED.Map.Astar.activePaths.value()).weight + (ED.Map.Astar.activePaths.value()).heuristic ) ){
                        ED.Map.Astar.BPSF = ED.Map.Astar.activePaths.value();
                    }
                    ED.Map.Astar.activePaths.next();
                }
            },
            AddConnections: function(treeNode){
                //var addedANode = false;
                
                //Only Add connections if treeNode is not destination
                if(treeNode.id != ED.Map.Astar.End.id){
                    
                    //Loop through treeNode Connections
                    for(var i = treeNode.node.Connections.length - 1; i >= 0; i--){
                        
                        //Create Tree Node
                        var tr = new CTreeNode(treeNode.node.id, ED.Map.Nodes.get(treeNode.node.Connections[i]));
                        
                        //Calculate Edge Weight and Heuristic Weight
                        tr.weight = treeNode.weight + ED.Map.Astar.GetDistance(treeNode, tr)
                        tr.heuristic = ED.Map.Astar.GetDistance(ED.Map.Astar.End, tr);
                        
                        //Test to see if node has already been visited
                        var visited = ED.Map.Astar.visitedNodes.get(tr.id);
                        
                        if(visited){ //Node has already been visited
                            
                            //If new path is shorter, replace in tree
                            if(visited.weight > tr.weight){
                                ED.Map.Astar.activePaths.put(tr.id, tr);
                                ED.Map.Astar.visitedNodes.put(tr.id, tr);
                            }
                        }
                        else{       //First time node has been visited
                            ED.Map.Astar.activePaths.put(tr.id, tr);
                            ED.Map.Astar.visitedNodes.put(tr.id, tr);
                            //addedANode = true;
                        }
                    }
                    
                    //Remove the treeNode from Active Paths
                    //  if no connection added, dead end
                    //  if a connection was added, then only need to search children
                    ED.Map.Astar.activePaths.remove(treeNode.id);
                    
                }
            }
        }
    }
}

var CNA = function(){
    //Initialize Emergency Department Simulation
    ED.Init();
    
}

var CNode = function(sName, iID, vPosition, vConnections){
    var self = this;
    this.name = sName;
    this.id = iID;
    this.Position = vPosition;
    this.Connections = vConnections;
    
    ED.Map.Nodes.put(iID, this);
    if(sName){ED.Map.Nodes.put(sName, this);}
}

var CTreeNode = function(parentID, node){
    var self = this;
    this.id = node.id;
    this.Children = node.Connections;
    this.parent = parentID;
    this.node = node;
    this.weight = 0;
    this.heuristic = 0;
}

var CRoom = function(sName){
    var self = this;
    this.occupied = false;
    this.name = sName;
    this.node = ED.Map.Nodes.get(sName);
    this.position = this.node.Position;
}

var COrder = function(sName){
    this.name = sName;
    this.time = os.graphics.Time.current;
}

