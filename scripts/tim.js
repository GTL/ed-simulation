// Trasition Probability Matrix

// Paired Data Object
function TPMEntry(state, probability)
{
    // state is the keyed value that is either looked up or returned 
    this.state = state;
    this.probability = probability;
        
}

// Open sourced by Ben Nadel
// http://www.bennadel.com/blog/1504-Ask-Ben-Parsing-CSV-Strings-With-Javascript-Exec-Regular-Expression-Command.htm
// This will parse a delimited string into an array of
// arrays. The default delimiter is the comma, but this
// can be overriden in the second argument.

function _CSVToArray( strData, strDelimiter )
{
    // Check to see if the delimiter is defined. If not,
    // then default to comma.
    strDelimiter = (strDelimiter || ",");

    // Create a regular expression to parse the CSV values.
    var objPattern = new RegExp(
            (
                    // Delimiters.
                    "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

                    // Quoted fields.
                    "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

                    // Standard fields.
                    "([^\"\\" + strDelimiter + "\\r\\n]*))"
            ),
            "gi"
            );


    // Create an array to hold our data. Give the array
    // a default empty first row.
    var arrData = [[]];

    // Create an array to hold our individual pattern
    // matching groups.
    var arrMatches = null;


    // Keep looping over the regular expression matches
    // until we can no longer find a match.
    while (arrMatches = objPattern.exec( strData )){

            // Get the delimiter that was found.
            var strMatchedDelimiter = arrMatches[ 1 ];

            // Check to see if the given delimiter has a length
            // (is not the start of string) and if it matches
            // field delimiter. If id does not, then we know
            // that this delimiter is a row delimiter.
            if (
                    strMatchedDelimiter.length &&
                    (strMatchedDelimiter != strDelimiter)
                    ){

                    // Since we have reached a new row of data,
                    // add an empty row to our data array.
                    arrData.push( [] );

            }


            // Now that we have our delimiter out of the way,
            // let's check to see which kind of value we
            // captured (quoted or unquoted).
            if (arrMatches[ 2 ]){

                    // We found a quoted value. When we capture
                    // this value, unescape any double quotes.
                    var strMatchedValue = arrMatches[ 2 ].replace(
                            new RegExp( "\"\"", "g" ),
                            "\""
                            );

            } else {

                    // We found a non-quoted value.
                    var strMatchedValue = arrMatches[ 3 ];

            }


            // Now that we have our value string, let's add
            // it to the data array.
            arrData[ arrData.length - 1 ].push( strMatchedValue );
    }

    // Return the parsed data.
    return( arrData );
}

// PRNG Random Number Generator
function RandNum()
{
    var self = this;
    this.Seed = 42;
    
    var LCG = function()
    {
        var _Modulus = 2147483647;
        var _Increment = 0;
        var _Multiplier = 16807;
    
        self.Seed = (_Multiplier * self.Seed + _Increment) % _Modulus;
    
        var randNum = self.Seed / _Modulus;
        
        return randNum;
    }
    
    this.RandBetweenAandB = function(a,b)
    {
            
       assert( a < b );
       return a + ( b - a ) * LCG();
    }
    
    this.RandBetween0and1 = function()
    {
        var number = LCG();
        return number;
    }
    
    

}


//var myObject = {
//    property1: 1,
//    property2: [],
//    property3: new MyClass(),
//    method1: function(){
//        
//    },
//    method2: function(){
//        
//    }
//}

// TPM Manager Class
var TPMMGR = {
    
    _TPM: undefined,
    
    _RNG: new RandNum(),
    
    tempVal: 0,
    
    // member function- passes in current state, and returns next state id
    GetNextState: function(currentState)
    {
        
        // check and see if currentState is a string -
        //if it is, look up the number and convert it
        if (isNaN(currentState))
        {
            //currentState is not a number, look up
            //in TPMMGR._TPM to find numeric value and reassign
            TPMMGR.tempVal = TPMMGR._TPM[0].indexOf(currentState);
            
            // indexOf returns -1 if search term isn't found,
            //throw an error to let user know and return same state (don't change)
            if (TPMMGR.tempVal == -1)
            {
                os.windows.Create.ErrorWindow('Invalid Entry',"An invalid state name was entered: " + currentState + "<br/><br/>State staying the same.");
                return currentState;
            }
            
            // if state found, reassign and continue
            currentState = TPMMGR.tempVal;
            
        }
        
        //generate rand num between 0 and 1
        var _rand = TPMMGR._RNG.RandBetween0and1();
        
        var _value = 0;
        
        var _i = 0;
                
        while (_value < _rand)
        {
            _value += TPMMGR._TPM[currentState][_i].probability;
            
            ++ _i;
        }
        
        // print current and next state
        os.console.AppendComment("Current State: " + currentState + '\n');
        os.console.AppendComment("Probability Rolled: " + _rand + '\n');
        os.console.AppendComment("Next State: " + TPMMGR._TPM[currentState][_i-1].state + '\n');
        
        return TPMMGR._TPM[currentState][_i-1].state;
    },
    
   // ImportNames Function - gets Name data and goes through table and links names with the states
   ImportNames: function(sFilename)
   {
        var self = this;
        var xhr = os.resschmgr.Create.XHRObject();
        var filepath = sFilename;
        var tempNameArray;
            
            
        // loads file from server/local space
        var _Load = function(filepath)
        {
            xhr.open('GET',filepath,false);
            xhr.onreadystatechange = function()
            {
                
                if(xhr.readyState==4)
                { //4==DONE
                    if(xhr.status == 200)
                    {
                       _Parse(xhr.responseText); 
                    }
                    else
                    {
                        os.windows.Create.ErrorWindow('Load Failure', "Failed to load TPM: URL: " + filepath + "<br/><br/>Check url and filename");
                    }
                    
                }
                                
            };
                        
            xhr.send();
            
        }
        
        var _Parse = function(fileContents)
        {
            
            //temp value
            var _tempNum;
            
            // put TPM into temp var
            var _tempTPM = _CSVToArray(fileContents);
            
            // delete top row
            //_tempTPM.splice(0,1);
            
            // delete the bottom row
            _tempTPM = _tempTPM.slice(0,-1);
            
            
           // if there are already numbers in the array
            if (undefined == TPMMGR._TPM)
            {
               TPMMGR._TPM = [[]];
            }
           
            
            for (var x = _tempTPM.length - 1; x > 0 ; --x)
            {
                TPMMGR._TPM[0][x] = _tempTPM[x][0];   
                
            }
            
                        
            //Anchor Console Open so you can see output
            os.debugbar.AnchorConsole();
            
            //Print contents to console for your view
            for (var i = 1; i <= TPMMGR._TPM[0].length - 1; i++)
            {
                os.console.Comment("State Name: " + TPMMGR._TPM[0][i] + ", State Number: " + i );
                
            }
            
                      
        }
        
        // both loads and parses the file into a 2D array
        _Load(sFilename);
   },
   
    // ImportTPM function - gets TPM data - calls Load and Parse private functions
    ImportTPM : function(sFilename)
    {
        var self = this;
        var xhr = os.resschmgr.Create.XHRObject();
        var filepath = sFilename;
            
            
        // loads file from server/local space
        var _Load = function(filepath)
        {
            xhr.open('GET',filepath,false);
            xhr.onreadystatechange = function()
            {
                
                if(xhr.readyState==4)
                { //4==DONE
                    if(xhr.status == 200)
                    {
                       _Parse(xhr.responseText); 
                    }
                    else
                    {
                        os.windows.Create.ErrorWindow('Load Failure', "Failed to load TPM: URL: " + filepath + "<br/><br/>Check url and filename");
                    }
                    
                }
                                
            };
                        
            xhr.send();
            
        }
        
        
               
        var _Parse = function(fileContents)
        {
            //temp value
            var _tempNum;
            
            // put TPM into temp var
            var _tempTPM = _CSVToArray(fileContents);
            
            // clear the top row
            _tempTPM[0] = [];
            
            // delete the bottom row
            _tempTPM = _tempTPM.slice(0,-1);
            
            // cut the first column, leaving the top row alone as its empty
            for (var i = _tempTPM.length-1; i > 0; --i)
            {
                // splice the first column
                _tempTPM[i].splice(0,1);
            }
        
            // creates TPMEntry objects and removes 0 entries
            for (var i = _tempTPM.length-1; i >= 0; --i)
                for (var j = _tempTPM[i].length - 1; j >= 0; --j)
                {
                    //convert value to number
                    _tempNum = parseFloat(_tempTPM[i][j]);
                    
                    // if number greater than 0, store it along with its state in a TPMEntry
                    if (_tempNum > 0){
                        _tempTPM[i][j] = new TPMEntry(j+1,_tempNum);
                    }
                     // otherwise, cut out the entry and move on
                     else{
                        _tempTPM[i].splice(j,1);
                     }
                    
                }
            
            // if there are already names in the array
            if (undefined != TPMMGR._TPM)
            {
                // insert TPM data, leaving row 0 as is.
                for (var x = _tempTPM.length-1; x > 0; --x)
                {
                    for (var y = _tempTPM[x].length-1; y >= 0; --y)
                    {
                        // convert data to number before storing in TPMMGR._TPM
                        TPMMGR._TPM[x][y] = _tempTPM[x][y];
                    }
                                        
                }
            }
            // if names aren't loaded
            else
            {
                TPMMGR._TPM = _tempTPM;    
            }
            
                        
            //Anchor Console Open so you can see output
            os.debugbar.AnchorConsole();
            
            //Print contents to console for your view
            for (var i = 1; i <= TPMMGR._TPM.length; i++)
            {
                os.console.Comment("\nState " + i + " Probabilities: " + TPMMGR._TPM[i].length )
                // comment following two lines out to just see the states and # of states they can transition to
                for (var j = 0; j < TPMMGR._TPM[i].length; j++)
                    os.console.AppendComment(TPMMGR._TPM[i][j].state + "," + TPMMGR._TPM[i][j].probability + ";");
                    
            }

        }
        
        // both loads and parses the file into a 2D array
        _Load(sFilename);
        
        
    } 
    
   

     
   
   
    
}

//// TPM Manager Class
//function TPMManager()
//{
//    
//
//   // stores final TPM data needed by accessor methods - is a 2d array
//   var TPMMGR._TPM;
//   // 
//   var TPMMGR._TPMSize = 0;
//   
//   // rand number generator
//   var _RNG = new RandNum();
//   
//   var TPMMGR.tempVal;
//   
//   // member function- passes in current state, and returns next state id
//   this.GetNextState = function(currentState)
//   {
//        
//        // check and see if currentState is a string -
//        //if it is, look up the number and convert it
//        if (isNaN(currentState))
//        {
//            //currentState is not a number, look up
//            //in TPMMGR._TPM to find numeric value and reassign
//            TPMMGR.tempVal = TPMMGR._TPM[0].indexOf(currentState);
//            
//            // indexOf returns -1 if search term isn't found,
//            //throw an error to let user know and return same state (don't change)
//            if (TPMMGR.tempVal == -1)
//            {
//                os.windows.Create.ErrorWindow('Invalid Entry',"An invalid state name was entered: " + currentState + "<br/><br/>State staying the same.");
//                return currentState;
//            }
//            
//            // if state found, reassign and continue
//            currentState = TPMMGR.tempVal;
//            
//        }
//        
//        //generate rand num between 0 and 1
//        var _rand = _RNG.RandBetween0and1();
//        
//        var _value = 0;
//        
//        var _i = 0;
//                
//        while (_value < _rand)
//        {
//            _value += TPMMGR._TPM[currentState][_i].probability;
//            
//            ++ _i;
//        }
//        
//        // print current and next state
//        os.console.AppendComment("Current State: " + currentState + '\n');
//        os.console.AppendComment("Probability Rolled: " + _rand + '\n');
//        os.console.AppendComment("Next State: " + TPMMGR._TPM[currentState][_i-1].state + '\n');
//        
//        return TPMMGR._TPM[currentState][_i-1].state;
//   }
//   // ImportNames Function - gets Name data and goes through table and links names with the states
//   this.ImportNames = function(sFilename)
//   {
//        var self = this;
//        var xhr = os.resschmgr.Create.XHRObject();
//        var filepath = sFilename;
//        var tempNameArray;
//            
//            
//        // loads file from server/local space
//        var _Load = function(filepath)
//        {
//            xhr.open('GET',filepath,false);
//            xhr.onreadystatechange = function()
//            {
//                
//                if(xhr.readyState==4)
//                { //4==DONE
//                    if(xhr.status == 200)
//                    {
//                       _Parse(xhr.responseText); 
//                    }
//                    else
//                    {
//                        os.windows.Create.ErrorWindow('Load Failure', "Failed to load TPM: URL: " + filepath + "<br/><br/>Check url and filename");
//                    }
//                    
//                }
//                                
//            };
//                        
//            xhr.send();
//            
//        }
//        
//        var _Parse = function(fileContents)
//        {
//            
//            //temp value
//            var _tempNum;
//            
//            // put TPM into temp var
//            var _tempTPM = _CSVToArray(fileContents);
//            
//            // delete top row
//            //_tempTPM.splice(0,1);
//            
//            // delete the bottom row
//            _tempTPM = _tempTPM.slice(0,-1);
//            
//            
//           // if there are already numbers in the array
//            if (undefined == TPMMGR._TPM)
//            {
//               TPMMGR._TPM = [[]];
//            }
//           
//            
//            for (var x = _tempTPM.length - 1; x > 0 ; --x)
//            {
//                TPMMGR._TPM[0][x] = _tempTPM[x][0];   
//                
//            }
//            
//                        
//            //Anchor Console Open so you can see output
//            os.debugbar.AnchorConsole();
//            
//            //Print contents to console for your view
//            for (var i = 1; i <= TPMMGR._TPM[0].length - 1; i++)
//            {
//                os.console.Comment("State Name: " + TPMMGR._TPM[0][i] + ", State Number: " + i );
//                
//            }
//            
//                      
//        }
//        
//        // both loads and parses the file into a 2D array
//        _Load(sFilename);
//   }
//   
//    // ImportTPM function - gets TPM data - calls Load and Parse private functions
//    this.ImportTPM = function(sFilename)
//    {
//        var self = this;
//        var xhr = os.resschmgr.Create.XHRObject();
//        var filepath = sFilename;
//            
//            
//        // loads file from server/local space
//        var _Load = function(filepath)
//        {
//            xhr.open('GET',filepath,false);
//            xhr.onreadystatechange = function()
//            {
//                
//                if(xhr.readyState==4)
//                { //4==DONE
//                    if(xhr.status == 200)
//                    {
//                       _Parse(xhr.responseText); 
//                    }
//                    else
//                    {
//                        os.windows.Create.ErrorWindow('Load Failure', "Failed to load TPM: URL: " + filepath + "<br/><br/>Check url and filename");
//                    }
//                    
//                }
//                                
//            };
//                        
//            xhr.send();
//            
//        }
//        
//        
//               
//        var _Parse = function(fileContents)
//        {
//            //temp value
//            var _tempNum;
//            
//            // put TPM into temp var
//            var _tempTPM = _CSVToArray(fileContents);
//            
//            // clear the top row
//            _tempTPM[0] = [];
//            
//            // delete the bottom row
//            _tempTPM = _tempTPM.slice(0,-1);
//            
//            // cut the first column, leaving the top row alone as its empty
//            for (var i = _tempTPM.length-1; i > 0; --i)
//            {
//                // splice the first column
//                _tempTPM[i].splice(0,1);
//            }
//        
//            // creates TPMEntry objects and removes 0 entries
//            for (var i = _tempTPM.length-1; i >= 0; --i)
//                for (var j = _tempTPM[i].length - 1; j >= 0; --j)
//                {
//                    //convert value to number
//                    _tempNum = parseFloat(_tempTPM[i][j]);
//                    
//                    // if number greater than 0, store it along with its state in a TPMEntry
//                    if (_tempNum > 0){
//                        _tempTPM[i][j] = new TPMEntry(j+1,_tempNum);
//                    }
//                     // otherwise, cut out the entry and move on
//                     else{
//                        _tempTPM[i].splice(j,1);
//                     }
//                    
//                }
//            
//            // if there are already names in the array
//            if (undefined != TPMMGR._TPM)
//            {
//                // insert TPM data, leaving row 0 as is.
//                for (var x = tempTPM.length-1; x > 0; --x)
//                {
//                    for (var y = tempTPM[x].length-1; y >= 0; --y)
//                    {
//                        // convert data to number before storing in TPMMGR._TPM
//                        TPMMGR._TPM[x][y] = tempTPM[x][y];
//                    }
//                                        
//                }
//            }
//            // if names aren't loaded
//            else
//            {
//                TPMMGR._TPM = _tempTPM;    
//            }
//            
//                        
//            //Anchor Console Open so you can see output
//            os.debugbar.AnchorConsole();
//            
//            //Print contents to console for your view
//            for (var i = 1; i <= TPMMGR._TPM.length; i++)
//            {
//                os.console.Comment("\nState " + i + " Probabilities: " + TPMMGR._TPM[i].length )
//                // comment following two lines out to just see the states and # of states they can transition to
//                for (var j = 0; j < TPMMGR._TPM[i].length; j++)
//                    os.console.AppendComment(TPMMGR._TPM[i][j].state + "," + TPMMGR._TPM[i][j].probability + ";");
//                    
//            }
//
//        }
//        
//        // both loads and parses the file into a 2D array
//        _Load(sFilename);
//        
//        
//    }
//    
//}

var Tim = function()
{
    //var TPMMGR = new TPMManager();
    
    //os.console.Comment("TPM Manager Load/Parse Methods");
    
    // how to import a file
    TPMMGR.ImportTPM("data/TPM.csv");
    TPMMGR.ImportNames("data/Eventcodes.csv")
    
    // how to use the gestnextstate functionality
    TPMMGR.GetNextState(10);
    
    // shows can enter a state name
    TPMMGR.GetNextState("One:One");
    
    // shows error when entering bad name
    TPMMGR.GetNextState("Needs their towel");
    
    
    
    
    
        
}